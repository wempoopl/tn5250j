FROM wempoo/nodejs

ENV TERM xterm
ENV DEBIAN_FRONTEND noninteractive

# install PHP-FPM
RUN \
  sudo apt-get update && \
  curl -sL https://deb.nodesource.com/setup_5.x | bash - && \
  sudo apt-get -y install supervisor php5 php5-fpm php5-cli php5-dev \
  php5-redis php5-mongo php5-gd php5-curl php5-mcrypt php5-xdebug nodejs && \
  rm -rf /var/lib/apt/lists/*

# enable PHP mcrypt extension
RUN php5enmod mcrypt

# disable 'daemonize' in php5-fpm
RUN sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php5/fpm/php-fpm.conf

# display errors
RUN sed -i "s/display_errors = Off/display_errors = On/" /etc/php5/fpm/php.ini

# add APP_ENV variable
RUN printf "\nenv[APP_ENV] = \$APP_ENV\n" >> /etc/php5/fpm/pool.d/www.conf

# insert missing fastcgi param
RUN echo "fastcgi_param  SCRIPT_FILENAME    \$request_filename;" >> /etc/nginx/fastcgi_params

# copy supervisor services
COPY services /etc/supervisor/conf.d

# Define working directory.
WORKDIR /app

# run supervisor on container startup
CMD ["/usr/bin/supervisord", "--nodaemon"]
