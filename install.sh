#!/bin/bash

#sudo apt-get update

# install git if not installed
if ! [ -x "$(command -v git)" ]; then

    sudo apt-get install -y git

fi

# check if repo is up to date
git fetch origin

if [ "$(git log HEAD..origin/master --oneline)" != "" ]; then

    git pull orgin master
    sh install.sh
    exit 0

fi

# install docker if not installed
if ! [ -x "$(command -v docker)" ]; then

    curl -sSL https://get.docker.com/ | sh
    sudo usermod -aG docker $(whoami)

fi

# install docker-compose if not installed
if ! [ -x "$(command -v docker-compose)" ]; then

    curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` > ~/docker-compose
    chmod +x ~/docker-compose && sudo mv ~/docker-compose /usr/local/bin/docker-compose
    sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose --version | awk 'NR==1{print $NF}')/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"

fi

# download and build all containers
(cd docker && docker-compose up -d)

# remove application hosts if they exists
sudo sed -i '/tn5250\.local/d' /etc/hosts

# add application hosts
sudo sh -c "cat >> /etc/hosts" <<EOF

# tn5250.local hosts
127.0.0.1   tn5250.local

EOF

echo "DONE!"
