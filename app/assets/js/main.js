/*
    tn5250 emulator
    @authors: Dariusz Zabrzeński, Błażej Kwaśniak
*/

var config = {
    list: '.list',
    initialize: function() {
        var self = this;

        if(this.checkJava()) {
            $('.style2').removeClass('off');
            $('.style2').find('a').attr('href', 'script.bat');
        }

        if(this.readCookie('tn5250-installed')) {
            $('.style3').removeClass('off');
            $('.style3').find('a').attr('href', 'emulator.html');
        }

        $('.installed').click(function(e) {
	    e.preventDefault();
            self.createCookie('tn5250-installed', 1, { expires : 10 });
            window.open('../../script.bat');
        });

        $('.add').click(function() {
            $(this).addClass('position');

            if($('.sessions .session').length === 1) {
                $('.sessions .session').addClass('half');
            }

            var session = $('<div class="session half"></div>');

            session.append(self.addForm());

            $('.sessions').append(session);

            if($('.sessions .session').length === 4) {
                $('.add').remove();
            }
        });

        $(document).on('click', '.new', function() {
            var host = $('input[name=host]').val(),
                port = $('input[name=port]').val(),
                ssl = $('select[name=type]').val();

            if(host.length == 0 || port.length == 0)
            {
                alert('Uzupełnij wszystkie pola!');
            }
            else if(!port.match(/^\d+$/))
            {
                alert('Podano nieprawidłowy port');
            }
            else {
                $('.add').show();
                self.addApplet(host, port, ssl);
            }
        });
    },
    newParam: function(name, value) {
        return '<PARAM NAME="'+name+'" VALUE="'+value+'" />';
    },
    addApplet: function(host, port, ssl) {
        var applet = $('<APPLET>', {
            CODE: 'org.tn5250j.My5250Applet.class',
            CODEBASE: '.',
            ARCHIVE: 'tn5250/tn5250j.jar',
            width: '100%',
            height: '100%',
            NAME: 'tn5250 - Java tn5250 Client'
        });

        applet
            .append(this.newParam('CODE', 'org.tn5250j.My5250Applet.class'))
            .append(this.newParam('CODEBASE', '.'))
            .append(this.newParam('NAME', 'tn5250 - Java tn5250 Client'))
            .append(this.newParam('type', 'application/x-java-applet;jpi-version=1.4'))
            .append(this.newParam('scriptable', 'false'))
            .append(this.newParam('host', host))
            .append(this.newParam('-p', port))
            .append(this.newParam('-sslType', ssl));


        $('.sessions .session').last().empty().append(applet);
    },
    addForm: function() {
        var form = $('<div>').addClass('form');

        form.append('<h3>Parametry połączenia</h3>')
            .append('<input type="text" name="host" placeholder="Host, e.g. 156.17.43.149" />')
            .append('<input type="text" name="port" placeholder="Port, e.g. 32023" />');

        var select = $('<select name="type">');

        select
            .append('<option value="" disabled selected>Typ SSL</option>')
            .append('<option value="">NONE</option>')
            .append('<option value="TLS">TLS</option>')
            .append('<option value="SSLv2">SSLv2</option>')
            .append('<option value="SSLv3">SSLv3</option>');

        form
            .append(select)
            .append('<div class="new">POŁĄCZ</div>');

        return form;
    },
    checkJava: function() {
        if (deployJava.versionCheck('1.6.0+') || deployJava.versionCheck('1.4') || deployJava.versionCheck('1.5.0*')) {
            return true;
        } else {
            return false;
        }
    },
    createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    },
    readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    },
    eraseCookie(name) {
        createCookie(name, "", -1);
    }
};

$(function() {
    config.initialize();
});
